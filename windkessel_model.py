from sys import argv
from os import system
from pylab import *
plt.rcParams.update({
    "text.usetex": True,
    "font.family": "mathplazo"
})

# --- Build directories
system("mkdir -p assets inflow")

# --- Get inflow
if len(argv)<2:
    T  = reshape(linspace(0,0.8*5,201*5), (201*5,1))
    Q  = reshape( 5700 - 500*cos(2*pi*(1.0/0.8)*T), (201*5,1) )
    DT = concatenate((T,Q),axis=1)
    savetxt("./inflow/inflow.dat", DT)
else:
    DT = loadtxt(argv[1])
    Q  = DT[:,1]

# --- Settings
T  = DT[:,0]
dt = DT[1,0];
Rp = 6.26e-6;
Rd = 2e-6;
R  = Rp+Rd;
C  = 3e5;
L  = R*0.1  ;
#dQ = (Q[1]-Q[0])/dt;
dQ = 0;

# --- Pressure init.
Pr   = zeros(len(Q)); # R
Prcr = zeros(len(Q)); # RCR
Prcrl= zeros(len(Q)); # RCRL

Pr[0]   = R*Q[0];
Prcr[0] = R*Q[0];
Prcrl[0]= R*Q[0];

# --- Time loop
for k in range(1,len(Pr)):

    # Purely resistive
    Pr[k]   = R*Q[k];

    # integral(0,t) Q(s) exp((s-t)/(RC))
    intQ = 0.5*Q[0]*exp(-(k*dt)/(Rd*C));
    for j in range(1,k):
        intQ += Q[j]*exp(((j-k)*dt)/(Rd*C));
    intQ += 0.5*Q[k];
    
    # RCR
    Pd      = Q[k] - Q[0]*exp(-(k*dt)/(Rd*C));
    Prcr[k] = Prcr[0]*exp(-(k*dt)/(Rd*C)) + Rp*Pd + (1/C)*dt*intQ;
    
    # RCRL
    ddQ      = (Q[k]-Q[k-1])/dt - dQ*exp(-(k*dt)/(Rd*C));
    Prcrl[k] = Prcr[k] + L*ddQ; 


# --- Display pressure
fg = figure(1,figsize=(6,2),constrained_layout=True)

plot(T,Pr,':')
plot(T,Prcr)
plot(T,Prcrl)

legend(['R','RCR','RCRL'])

title("Pressure at interface $P(t)$")
xlabel("Time $[s]$")
ylabel("Pressure $[kg.mm^{-1}.s^{-2}]$")
grid(True, linestyle=':')

if len(argv)>2:
    save_path = "./assets/fig_pressure_"+argv[2]+".pdf"
else:
    save_path = "./assets/fig_pressure.pdf"
fg.savefig(save_path);

# --- Display flow rate
fg = figure(2,figsize=(6,2),constrained_layout=True)

plot(T,Q)

title("Flowrate $Q(t)$")
xlabel("Time $[s]$")
ylabel("Flowrate $[mm^3.s^{-1}]$")
grid(True, linestyle=':')

if len(argv)>2:
    save_path = "./assets/fig_flowrate_"+argv[2]+".pdf"
else:
    save_path = "./assets/fig_flowrate.pdf"
fg.savefig(save_path);
