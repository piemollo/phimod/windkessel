# Toy problem Windkessel model

This repository contains a python script to simulate 0D 
R/RCR/RCRL Windkessel models.
It only requires a standard `python` install.

## How to use it

First download the repository using the command
````bash 
git clone https://gitlab.com/piemollo/phimod/windkessel.git
````
then one can access the corresponding directory and directly
runs the script
````bash
cd windkessel
python windkessel_model.py
````
Note that the first execution of the script will build two
directories `assets/` and `inflow/` in the current directory.
The former will contain output pdf images and the latter will 
contain the model inflow data.

## Use a custom inflow

In this context, an inflow is a 2 columns array stored in ASCII
format.
The first column contains time information and the second 
one contains the actual flow information.
There is an option to automatically put a label to output pdf images.
The associated command reads
````bash
python windkessel_model <path_to_inflow> <lab>
````

As an example, if one wants to use a custom inflow `my_inflow.dat` 
placed in the `inflow/` directory and want the images to have the 
label `my_label`, the corresponding command reads
````bash
python windkessel_model ./inflow/my_inflow.dat my_label
````
If the code runs correctly, the output should be two images 
`fig_pressure_my_label.pdf` and `fig_flowrate_my_label.pdf`
located in the `assets/` folder.

Values of the Windkessel components can be changed directly inside 
the code to compare different setup.